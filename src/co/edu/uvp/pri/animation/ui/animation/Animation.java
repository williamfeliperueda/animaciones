package co.edu.uvp.pri.animation.ui.animation;

import java.awt.Color;
import java.awt.Graphics;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;

public class Animation extends JComponent implements Runnable {

    private Thread thread = null;
    private final static  short BALL_WIDTH = 50;
    private int bx,by =0;
    private int dx = 2;
    private int dy=2;
    
        
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        // solamente lógica de dibujar  
        g.setColor(Color.GREEN);
        g.fillOval(bx, by,BALL_WIDTH ,BALL_WIDTH);
        
        
    }

    @Override
    public void run() {
        while (this.thread != null) {
            // Lógica de la animación - modificar variables
//               if((bx >= getWidth() - BALL_WIDTH)||(bx < 0)){
//                   dx= -dx;}
//               
//               bx +=dx;
               
            if((bx >= getWidth() - BALL_WIDTH)||(bx < 0)){
                dx = -dx;//cambio de signo
            }
            bx += dx;
            
            
            if((by >= getHeight() - BALL_WIDTH)||(by <0)){
                dy =-dy;
            }
            by+= dy;
            
            
            repaint();
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(Animation.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void init() {
        if (this.thread == null) {
            this.thread = new Thread(this);
            this.thread.start();
        }
    }

    public void pause() {
        this.thread = null;
    }

    public void restart() {
        // Iniciar variables
        this.init();
    }

}
